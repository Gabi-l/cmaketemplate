#ifndef B_H_
#define B_H_

class B
{
  public:
  B() = default;

  void Print();

  private:
  int _count = 0;
};

#endif  // B_H_