#ifndef A_H_
#define A_H_

class A
{
  public:
  A() = default;

  void Print();

  private:
  int _count = 0;
};

#endif  // A_H_